<x-app-layout>
</x-app-layout> 

<!DOCTYPE html>
<html lang="en">
  <head>
    @include("admin.admincss")
  </head>
  <body>
    <div class="container-scroller">
    @include("admin.navbar")
    <div style="position: relative; top:-500px; right:-245px">
        @if(session()->has('message'))
        <div class="alert alert-success" role="alert">
            <button type="button" class="btn-close" aria-label="close"></button>
            {{ session()->get('message') }}
        </div>
         @endif
        <form action="{{ url('/uploadchef') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div style="padding: 15px;">
                <label>Name:</label>
                <input style="color: blue" type="text" name="name" placeholder="Enter Name" required>
            </div>
            <div style="padding: 15px;">
                <label>Speciality</label>
                <input style="color: blue" type="text" name="speciality" placeholder="Enter Speciality" required>
            </div>
            <div style="padding: 15px;">
                <label>Image</label>
                <input type="file" name="image" required>
            </div>
            <div style="padding: 15px;">
                <input class="btn btn-success" type="submit" value="Save">
            </div>
        </form>


        <div style="position: relative; top:20px; right:-120px">
            <table style="background-color: black">
                <tr>
                    <td style="padding: 30px;">Name</td>
                    <td style="padding: 30px;">Speciality</td>
                    <td style="padding: 30px;">Image</td>
                    <td align="center" style="padding: 30px">Action</td>
                </tr>

                @foreach ($data as $chefupdate)
                <tr align="center">
                    <td>{{ $chefupdate->name }}</td>
                    <td>{{ $chefupdate->speciality }}</td>
                    <td><img height="100" width="100" src="/chefimage/{{ $chefupdate->image }}"></td>
                    <td><a href="{{ url('/deletechef',$chefupdate->id) }}">Delete</a></td>
                    <td><a href="{{ url('/updatechef',$chefupdate->id) }}">Edit</a></td>
                </tr>
                @endforeach

            </table>
        </div>

    </div>
    </div>
    @include("admin.adminscript")
  </body>
</html>