<x-app-layout>
</x-app-layout> 

<!DOCTYPE html>
<html lang="en">
  <head>

    <base href="/public">
    @include("admin.admincss")
  </head>
  <body>
    <div class="container-scroller">
        @include("admin.navbar")
        <div style="position: relative; top:-500px; right:-500px">
            @if(session()->has('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="btn-close" aria-label="close"></button>
                    {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ url('/update',$data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div style="padding: 15px;">
                    <label>Title:</label>
                    <input style="color: blue" type="text" name="title" value="{{ $data->title }}" required>
                </div>
                <div style="padding: 15px;">
                    <label>Price</label>
                    <input style="color: blue" type="number" name="price" value="{{ $data->price }}" required>
                </div>
                <div style="padding: 15px;">
                    <label>Old Image</label>
                    <img height="100" width="100" src="/foodimage/{{ $data->image }}">
                </div>
                <div style="padding: 15px;">
                    <label>New Image</label>
                    <input type="file" name="image">
                </div>
                <div style="padding: 15px;">
                    <label>Description</label>
                    <input style="color: blue" type="text" name="description" value="{{ $data->description }}" required>
                </div>
                <div style="padding: 15px;">
                    <input class="btn btn-success" type="submit" value="Save">
                </div>
            </form>
    </div>
    </div>
        @include("admin.adminscript")
  </body>
</html>