<x-app-layout>
</x-app-layout> 

<!DOCTYPE html>
<html lang="en">
  <head>

    <base href="/public">
    @include("admin.admincss")
  </head>
  <body>
    <div class="container-scroller">
        @include("admin.navbar")
        <div style="position: relative; top:-500px; right:-500px">
            @if(session()->has('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="btn-close" aria-label="close"></button>
                    {{ session()->get('message') }}
                </div>
            @endif
            <form action="{{ url('/updatefoodchef',$data->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div style="padding: 15px;">
                    <label>Name:</label>
                    <input style="color: blue" type="text" name="name" value="{{ $data->name }}" required>
                </div>
                <div style="padding: 15px;">
                    <label>Speciality</label>
                    <input style="color: blue" type="text" name="speciality" value="{{ $data->speciality }}" required>
                </div>
                <div style="padding: 15px;">
                    <label>Old Image</label>
                    <img height="100" width="100" src="/chefimage/{{ $data->image }}">
                </div>
                <div style="padding: 15px;">
                    <label>New Image</label>
                    <input type="file" name="image">
                </div>
                <div style="padding: 15px;">
                    <input class="btn btn-success" type="submit" value="Save">
                </div>
            </form>
    </div>
    </div>
        @include("admin.adminscript")
  </body>
</html>