<x-app-layout>
</x-app-layout> 

<!DOCTYPE html>
<html lang="en">
  <head>
    @include("admin.admincss")
  </head>
  <body>
    <div class="container-scroller">
    @include("admin.navbar")
        <div style="position: relative; top:-500px; right:-245px">
            <table style="background-color: black">
                <tr>
                    <td style="padding: 30px;">Name</td>
                    <td style="padding: 30px;">Email</td>
                    <td style="padding: 30px;">Phone</td>
                    <td style="padding: 30px;">Guest</td>
                    <td style="padding: 30px;">Date</td>
                    <td style="padding: 30px;">Time</td>
                    <td style="padding: 30px;">Message</td>
                    <td align="center" style="padding: 30px">Action</td>
                </tr>

                @foreach ($data as $reservation)
                <tr align="center">
                    <td>{{ $reservation->name }}</td>
                    <td>{{ $reservation->email }}</td>
                    <td>{{ $reservation->phone }}</td>
                    <td>{{ $reservation->guest }}</td>
                    <td>{{ $reservation->date }}</td>
                    <td>{{ $reservation->time }}</td>
                    <td>{{ $reservation->message }}</td>
                    {{-- <td><a href="{{ url('/deletemenus',$foodmenus->id) }}">Delete</a></td>
                    <td><a href="{{ url('/updateview',$foodmenus->id) }}">Edit</a></td> --}}
                </tr>
                @endforeach

            </table>
        </div>
    </div>
    @include("admin.adminscript")
  </body>
</html>