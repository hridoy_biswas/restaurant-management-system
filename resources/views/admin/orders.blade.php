<x-app-layout>
</x-app-layout> 

<!DOCTYPE html>
<html lang="en">
  <head>
    @include("admin.admincss")
  </head>
  <body>
    <div class="container-scroller">
    @include("admin.navbar")
    <h1 style="position: relative; top:-500px; right:-245px">Customer Order</h1>
    <div style="position: relative; top:-500px; right:-145px"> 
        <div class="container">
            <form action="{{ url('/search') }}" method="get">
                @csrf
                <input type="text" name="search" style="color:blue;">
                <input type="submit" value="Search" class="btn btn-success">
            </form>
        <table style="background-color: black">
            <tr>
                <td style="padding: 30px;">Name</td>
                <td style="padding: 30px;">Phone</td>
                <td style="padding: 30px;">Address</td>
                <td style="padding: 30px;">FoodName</td>
                <td style="padding: 30px;">Price</td>
                <td style="padding: 30px;">Quantity</td>
                <td align="center" style="padding: 30px">Total Price</td>
            </tr>

            @foreach ($data as $data)
            <tr align="center">
                <td>{{ $data->name }}</td>
                <td>{{ $data->phone }}</td>
                <td>{{ $data->address }}</td>
                <td>{{ $data->foodname }}</td>
                <td>TK: {{ $data->price }}</td>
                <td>{{ $data->quantity }}</td>
                <td>TK: {{ $data->price * $data->quantity }}</td>
                {{-- <td><a href="{{ url('/deletemenus',$foodmenus->id) }}">Delete</a></td>
                <td><a href="{{ url('/updateview',$foodmenus->id) }}">Edit</a></td> --}}
            </tr>
            @endforeach

        </table>
    </div>
    </div>
    </div>
    @include("admin.adminscript")
  </body>
</html>