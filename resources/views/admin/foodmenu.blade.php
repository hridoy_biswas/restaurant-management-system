<x-app-layout>
</x-app-layout> 

<!DOCTYPE html>
<html lang="en">
  <head>
    @include("admin.admincss")
  </head>
  <body>
    <div class="container-scroller">
        @include("admin.navbar")
        <div style="position: relative; top:-500px; right:-500px">
            @if(session()->has('message'))
                <div class="alert alert-success" role="alert">
                    <button type="button" class="btn-close" aria-label="close"></button>
                    {{ session()->get('message') }}
                </div>
            @endif
                <form action="{{ url('/uploadfood') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div style="padding: 15px;">
                        <label>Title:</label>
                        <input style="color: blue" type="text" name="title" placeholder="Enter title" required>
                    </div>
                    <div style="padding: 15px;">
                        <label>Price</label>
                        <input style="color: blue" type="number" name="price" placeholder="Enter Price" required>
                    </div>
                    <div style="padding: 15px;">
                        <label>Image</label>
                        <input type="file" name="image" required>
                    </div>
                    <div style="padding: 15px;">
                        <label>Description</label>
                        <input style="color: blue" type="text" name="description" placeholder="Enter Description" required>
                    </div>
                    <div style="padding: 15px;">
                        <input class="btn btn-success" type="submit" value="Save">
                    </div>
                </form>
        </div>


        <div style="position: relative; top:-500px; right:-500px">
            <table style="background-color: black">
                <tr>
                    <td style="padding: 30px;">Title</td>
                    <td style="padding: 30px;">Price</td>
                    <td style="padding: 30px;">Description</td>
                    <td style="padding: 30px;">Image</td>
                    <td align="center" style="padding: 30px">Action</td>
                </tr>

                @foreach ($data as $foodmenus)
                <tr align="center">
                    <td>{{ $foodmenus->title }}</td>
                    <td>{{ $foodmenus->price }}</td>
                    <td>{{ $foodmenus->description }}</td>
                    <td><img height="100" width="100" src="/foodimage/{{ $foodmenus->image }}"></td>
                    <td><a href="{{ url('/deletemenus',$foodmenus->id) }}">Delete</a></td>
                    <td><a href="{{ url('/updateview',$foodmenus->id) }}">Edit</a></td>
                </tr>
                @endforeach

            </table>
        </div>
    </div>
        @include("admin.adminscript")
  </body>
</html>