<?php

namespace App\Http\Controllers;

use App\Models\Food;
use App\Models\Foodchef;
use App\Models\Order;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    public function user()
    {
        $data = user::all();
        return view("admin.users",compact("data"));
    }

    public function deleteuser($id)
    {
        $data=user::find($id);
        $data->delete();
        return redirect()->back();
    }

    public function foodmenu()
    {
        $data = food::all();
        return view("admin.foodmenu",compact("data"));
    }

    public function deletemenus($id)
    {
        $data=food::find($id);
        $data->delete();
        return redirect()->back();
    }

    public function updateview($id)
    {
        $data=food::find($id);
        return view("admin.updateview",compact("data"));
    }

    public function update(Request $request, $id)
    {
        $data=food::find($id);

        $image=$request->image;

        $imagename=time().'.'.$image->getClientoriginalExtension();

        $request->image->move('foodimage',$imagename);

        $data->image=$imagename;

        $data->title=$request->title;
        $data->price=$request->price;
        $data->description=$request->description;

        $data->save();

        return redirect()->back()->with('message','Food Updated Successfully!!');

    }

    public function upload(Request $request)
    {
        $data = new food;

        $image=$request->image;

        $imagename=time().'.'.$image->getClientoriginalExtension();

        $request->image->move('foodimage',$imagename);

        $data->image=$imagename;

        $data->title=$request->title;
        $data->price=$request->price;
        $data->description=$request->description;

        $data->save();

        return redirect()->back()->with('message','Food Added Successfully!!');
    }


    public function reservation(Request $request)
    {
        $data = new reservation();

        $data->name=$request->name;
        $data->email=$request->email;
        $data->phone=$request->phone;
        $data->guest=$request->guest;
        $data->date=$request->date;
        $data->time=$request->time;
        $data->message=$request->message;

        $data->save();

        return redirect()->back()->with('message','Reservations Added Successfully!!!');
    }

    public function viewreservations()
    {
        if(Auth::id())
        {
        $data = reservation::all();

        return view("admin.adminreservation",compact("data"));
        }
        else
        {
            return redirect('login');
        }
    }

    public function viewchef()
    {
        $data=foodchef::all();
        return view("admin.adminchef",compact("data"));
    }

    public function uploadchef(Request $request)
    {
        $data = new foodchef;

        $image=$request->image;

        $imagename=time().'.'.$image->getClientoriginalExtension();

        $request->image->move('chefimage',$imagename);

        $data->image=$imagename;

        $data->name=$request->name;
        $data->speciality=$request->speciality;

        $data->save();
        return redirect()->back()->with('message','Chef Added Successfully');

    }

    public function updatechef($id)
    {
        $data=foodchef::find($id);
        return view("admin.updatechef",compact("data"));
    }

    public function updatefoodchef(Request $request, $id)
    {
        $data = foodchef::find($id);
        $image=$request->image;

        if($image)
        {
            $imagename=time().'.'.$image->getClientoriginalExtension();

            $request->image->move('chefimage',$imagename);
            
            $data->image=$imagename;
        }
        $data->name=$request->name;
        $data->speciality=$request->speciality;

        $data->save();
        return redirect()->back()->with('message','Chef Updated Succesfully');
    }

    public function deletechef($id)
    {
        $data=foodchef::find($id);
        $data->delete();
        return redirect()->back()->with('message','Chef Deleted Successfully');
    }

    public function orders()
    {
        $data=Order::all();
        return view("admin.orders", compact('data'));
    }

    public function search(Request $request)
    {
        $search=$request->search;
        $data=order::where('name','Like','%'.$search.'%')
        ->orWhere('foodname','Like','%'.$search.'%')
        ->orWhere('phone','Like','%'.$search.'%')
        ->get();
        return view("admin.orders", compact('data'));
    }
}
