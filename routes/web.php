<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get("/",[HomeController::class,"index"]);


Route::get("/users",[AdminController::class,"user"]);

Route::get("/foodmenu",[AdminController::class,"foodmenu"]);
Route::get("/deletemenus/{id}",[AdminController::class,"deletemenus"]);
Route::post("/uploadfood",[AdminController::class,"upload"]);
Route::post("/update/{id}",[AdminController::class,"update"]);

Route::get("/deleteuser/{id}",[AdminController::class,"deleteuser"]);
Route::get("/updateview/{id}",[AdminController::class,"updateview"]);


///Reservations

Route::post("/reservation",[AdminController::class,"reservation"]);
Route::get("/viewreservations",[AdminController::class,"viewreservations"]);



////chef

Route::get("/viewchef",[AdminController::class,"viewchef"]);
Route::post("/uploadchef",[AdminController::class,"uploadchef"]);
Route::get("/updatechef/{id}",[AdminController::class,"updatechef"]);
Route::post("/updatefoodchef/{id}",[AdminController::class,"updatefoodchef"]);
Route::get("/deletechef/{id}",[AdminController::class,"deletechef"]);



///addcart
Route::post("/addcart/{id}",[HomeController::class,"addcart"]);
Route::get("/showcart/{id}",[HomeController::class,"showcart"]);

Route::get("/remove/{id}",[HomeController::class,"remove"]);

Route::post("/orderconfirm",[HomeController::class,"orderconfirm"]);
Route::get("/orders",[AdminController::class,"orders"]);
Route::get("/search",[AdminController::class,"search"]);



Route::get("redirects",[HomeController::class,"redirects"]);

Route::middleware(['auth:sanctum','verified'])->get('/dashboard',function () 
    {
    return view('dashboard');
    })->name('dashboard');
